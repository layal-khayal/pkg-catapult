#!/bin/env python3
import os
import platform
import json
import logging
from contextlib import contextmanager
import time
import argparse
import itertools
import math
from dataclasses import dataclass, asdict
from typing import Sequence, List, Tuple, Optional, Dict, Union, Callable, Iterable
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile, gettempdir
from getpass import getpass
from libbiomedit.lib.deserialize import deserialize as sett_deserialize
from sett.workflows import encrypt, transfer
from sett.utils.config import Config as SettConfig
from sett.core.filesystem import delete_files
from sett.core.metadata import Purpose
from sett.core.secret import Secret
from sett.protocols.sftp import Protocol as sftp


APP_NAME = os.path.splitext(os.path.basename(__file__))[0]
PKG_DEFAULT_CONTENT = "test content"
logger = logging.getLogger(__file__)


@dataclass
class Connection:
    """SFTP connection parameters.

    :param host: URL of SFTP server to connect to.
    :param username: user name with which to connect to SFTP server.
    :param pkey: private SSH key to use for authentication when connecting to
        the host SFTP server.
    :param destination_dir: directory or host where to upload data.
    """

    host: str
    username: str
    pkey: Optional[str] = None
    destination_dir: str = "upload"


@dataclass
class Destination:
    """Data transfer parameters.

    :param id: name of destination (can be anything).
    :param connection: SFTP connection parameters of destination server.
    :param dtr_id: BiomedIT DTR number to use for the transfer.
    :param default_recipients: email address of data transfer recipient.
    """

    id: str
    connection: Connection
    dtr_id: int
    default_recipients: Sequence[str] = ()

    @classmethod
    def from_dict(cls, data: dict):
        try:
            return cls(connection=Connection(**data.pop("connection")), **data)
        except TypeError as e:
            raise ValueError(
                f"Invalid config: Missing / unexpected field(s) "
                f"encountered in some Destination: {e}"
            ) from e

    def with_recipients(self, recipients: Sequence[str]):
        if recipients:
            self.default_recipients = recipients
        return self


@dataclass
class Route:
    dest: Destination
    via: Destination  # can be the same as dest


@dataclass
class DataTransfer:
    """Class representing a sett "data tranfer", i.e. the packaging and
    transfer of a set of files. DataTransfer have the following attributes:

    :param sender: email/fingerprint of data sender.
    :param recipients: email/fingerprint of the data recipient(s).
    :param route: intended destination route of the transferred data.
    :param files: file(s) and/or directory(ies) to transfer.
    :param package_file: path of packaged (compressed + encrypted) data package.
    :param uncompressed_size: total size in bytes of the transferred files.
    :param compressed_size: size in bytes of package_file.
    :param encrypt_time: time in seconds it took to compress+encrypt the
        package.
    :param transfer_time: time in seconds it took to transfer the package.
    """

    # Required attributes that must be defined at creation.
    sender: str
    recipients: Sequence[str]
    route: Route
    files: Sequence[str]
    package_file: str

    # Attributes that get populated as the data is encrypted and transferred.
    uncompressed_size: int = 0
    compressed_size: int = 0
    encrypt_time: int = 0
    transfer_time: int = 0

    @property
    def route_str(self) -> str:
        """String representation of tranfer route"""
        via = self.route.via.id
        dest = self.route.dest.id
        return f"-> {via}{' -> ' + dest if dest != via else ''}"

    @property
    def stats_str(self) -> str:
        """One-line summary of transfer stats"""
        return (
            "file_size="
            + file_size_as_string(self.uncompressed_size, sep="")
            + " compressed_size="
            + file_size_as_string(self.compressed_size, sep="")
            + " encrypt_time="
            + str(timedelta(seconds=self.encrypt_time))
            + (
                " transfer_time=" + str(timedelta(seconds=self.transfer_time))
                if self.transfer_time
                else ""
            )
            + " files="
            + str(self.files).replace(" ", "")
        )

    @property
    def encrypt_time_str(self) -> str:
        """One-line summary of encryption time"""
        return (
            f"{self.route_str} -  encrypt: {str(timedelta(seconds=self.encrypt_time))}"
        )

    @property
    def transfer_time_str(self) -> str:
        """One-line summary of transfer time"""
        return (
            f"{self.route_str} - transfer: {str(timedelta(seconds=self.transfer_time))}"
        )


@dataclass
class Config:
    destinations: Dict[str, Destination]
    sett_cfg: SettConfig

    @classmethod
    def from_dict(cls, data: dict):
        destinations = data.pop("destinations", {})
        sett_cfg = data.pop("sett_cfg", {})
        try:
            return cls(
                destinations={
                    node_id: Destination.from_dict(dict(id=node_id, **dest))
                    for node_id, dest in destinations.items()
                },
                sett_cfg=sett_deserialize(SettConfig)(sett_cfg),
            )
        except TypeError as e:
            raise ValueError(
                f"Invalid config: Missing / unexpected field(s)"
                f" encountered in config: {e}"
            ) from e

    @classmethod
    def load(cls, config_file):
        """Parse config file and verify mandatory
        fields have been specified
        """
        with open(config_file, "r", encoding="utf-8") as stream:
            try:
                cfg = json.load(stream)
                return cls.from_dict(cfg)
            except (TypeError, json.JSONDecodeError) as err:
                raise ValueError(
                    f"Failed parsing {config_file}." f" Error {type(err)}:{err}"
                ) from err


class EmojiLevelFormatter(logging.Formatter):
    emojis = {
        logging.DEBUG: "🔬",
        logging.INFO: "ℹ️",
        logging.WARNING: "⚠️",
        logging.ERROR: "💥",
        logging.CRITICAL: "☢️",
    }

    def format(self, record):
        record.levelname = self.emojis.get(record.levelno, record.levelname)
        return logging.Formatter.format(self, record)


def main(*, report_file: Optional[str], create_only: bool, **kwargs):
    """Main function of the application"""

    # Compress, encrypt and transfer each file set along each route.
    data_transfers = list(process_jobs(create_only=create_only, **kwargs))

    # Display packaging and transfer time summary.
    logger.info(
        "Time log [H:M:S]:\n%s",
        "\n".join(time_stats(data_transfers, create_only=create_only)),
    )
    logger.info("All jobs completed.")

    # Write data packaging and transfer stats to report file, if requested.
    if report_file:
        logger.info("See [%s] for details", report_file)
        with open(report_file, "w", encoding="utf-8") as f_report:
            for data_transfer in data_transfers:
                f_report.write(
                    f"[{datetime.now().astimezone().strftime('%Y-%m-%d %H:%M:%S')}] "
                    f"{data_transfer.stats_str}\n"
                )


def process_jobs(
    routes: Sequence[Route],
    file_sets: Sequence[Sequence[str]],
    sender: Optional[str],
    recipients: Sequence[str],
    keep_pkg: bool,
    create_only: bool,
    sett_cfg: SettConfig,
    output_dir: str,
) -> Iterable[DataTransfer]:
    """Package (compress + encrypt) each file set and tranfer them via the
    specified routes.

    :param routes: list of routes through which to send each set of files.
    :param file_sets: one or more (if in batch mode) lists of files to process.
    :param sender: email or fingerprint of data sender. If None, the default
        sender defined in the config file or the default PGP key from the
        user's keyring is used as sender.
    :param recipients: list of one or more data recipients. If None, the
        default recipients defined for a given route in the config file or
        the default PGP key from the user's keyring is used as recipient(s).
    :param keep_pkg: if True, the encrypted sett packages are kept. If False,
        they are deleted after the job completes.
    :param create_only: if True, only create encrypted sett packages without
        transferring them.
    :param sett_cfg: sett configuration object.
    :param output_dir: output directory path.
    :return: generator of DataTransfers.
    """
    check_files_exist(tuple(itertools.chain(*file_sets)))

    # Set values for data sender and recipients. Values are set based on the
    # following precedence order:
    #  -> sender/recipient passed in arguments.
    #  -> default sender retrieved from the sett config file,
    #     default recipients as defined in the route's destination (which is
    #     stored in the pkg-catapult config file).
    #  -> default PGP key from local keyring.
    if not sender or not recipients:
        default_pgp_key = sett_cfg.gpg_store.default_key()
    else:
        default_pgp_key = None

    sender = sender or sett_cfg.default_sender or default_pgp_key
    if not sender:
        raise ValueError("no 'sender' value given and no defaults defined")

    assign_recipients(routes, recipients=recipients, default_pgp_key=default_pgp_key)
    sett_cfg.output_dir = output_dir

    # Ask user for PGP key password:
    sender_pwd = Secret(getpass(f"Please enter the password for gpg key {sender}:"))

    # For each route and job in the batch (or the unique job if not in batch
    # mode), create a DataTransfer instance.
    for n, job in enumerate(
        collect_data_transfer_jobs(routes, file_sets, sender, output_dir)
    ):
        if len(file_sets) > 1 or len(routes) > 1:
            logger.info("Starting job %i/%i", n, len(file_sets) * len(routes))

        # Encrypt and transfer data.
        yield process_pkg(
            data_transfer=job,
            sett_config=sett_cfg,
            sender_pwd=sender_pwd,
            keep_pkg=keep_pkg,
            create_only=create_only,
        )


def collect_data_transfer_jobs(
    routes: Sequence[Route],
    file_sets: Sequence[Sequence[str]],
    sender: str,
    output_dir: str,
) -> Iterable[DataTransfer]:
    """Creates a DataTransfer job for each file set and route."""

    for file_set in file_sets:
        for route in routes:
            yield DataTransfer(
                route=route,
                files=file_set,
                sender=sender,
                recipients=route.dest.default_recipients,
                package_file=os.path.join(
                    output_dir,
                    datetime.now().astimezone().strftime(encrypt.DATE_FMT_FILENAME)
                    + ".zip",
                ),
            )


def assign_recipients(
    routes: Sequence[Route], recipients: Sequence[str], default_pgp_key: Optional[str]
) -> None:
    """Assign the specified recipients or the destinations' default recipients
    to each route destination node. For each route, the recipients are assigned
    using the following order of precedence:
     * User specified recipients.
     * Default recipients of destination node (specified in the config file).
     * Default PGP key from the user's GPG keyring.

    As routes are mutable objects, their value is modified in-place by this
    function, which therefore retruns nothing.
    """
    default_pgp_keys = [default_pgp_key] if default_pgp_key else []
    for destination in (route.dest for route in routes):
        r = recipients or destination.default_recipients or default_pgp_keys
        if not r:
            raise ValueError(
                f"node_id {destination.id}: No recipients given and "
                f"no defaults defined"
            )
        destination.default_recipients = r


def time_stats(data_transfers, create_only: bool = False):
    for counter, data_transfer in enumerate(data_transfers, start=1):
        yield f"     job {counter}: {data_transfer.encrypt_time_str}"
        if not create_only:
            yield f"     job {counter}: {data_transfer.transfer_time_str}"


def process_pkg(
    data_transfer: DataTransfer,
    sett_config: SettConfig,
    sender_pwd: Secret[str],
    keep_pkg: bool = False,
    create_only: bool = False,
) -> DataTransfer:
    """Wrapper function to package and transfer data to the destination(s)
    specified in routes.

    :param data_transfer: data to package and transfer.
    :param sett_config: sett configuration.
    :param sender_pwd: password associated with sender PGP key.
    :param keep_pkg: if False, delete the encrypted data package after transfer.
    :param create_only: only create the data package, without transferring it.
    :return: input DataTransfer with updated attributes (size, time, etc.)
    :raises ValueError:
    """

    try:
        # Data encryption and compression.
        start_time = time.time()
        data_transfer = create_pkg(
            data_transfer=data_transfer,
            sender_pwd=sender_pwd,
            sett_config=sett_config,
        )
        data_transfer.encrypt_time = round(time.time() - start_time)

        if create_only:
            return data_transfer

        # Data transfer.
        start_time = time.time()
        send_pkg(
            data_transfer=data_transfer,
            sett_config=sett_config,
        )
        data_transfer.transfer_time = round(time.time() - start_time)

    finally:
        if not keep_pkg:
            delete_files(data_transfer.package_file)

    return data_transfer


def create_pkg(
    data_transfer: DataTransfer, sender_pwd: Secret[str], sett_config: SettConfig
) -> DataTransfer:
    """Create an encrypted data package with sett.

    :param data_transfer: data to be packaged.
    :param sender_pwd: password associated with sender PGP key.
    :param sett_config: sett configuration.
    :return: input DataTransfer with the following attributes updated:
        package_file, uncompressed_size, compressed_size
    :raises ValueError:
    """

    # Compute size of uncompressed data
    files = data_transfer.files
    data_transfer.uncompressed_size = (
        total_file_size(files) if files else len(PKG_DEFAULT_CONTENT) * 2
    )

    # Display package summary info.
    logger.info(
        "📦 Encrypting data with parameters:\n"
        "        - destination: %s\n"
        "        - sender     : %s\n"
        "        - recipients : %s\n"
        "        - DTR ID     : %s\n"
        "        - files      : %s\n"
        "        - total size : %s",
        data_transfer.route_str,
        data_transfer.sender,
        ", ".join(data_transfer.recipients),
        data_transfer.route.dest.dtr_id,
        ", ".join(os.path.basename(f) for f in files)
        if files
        else "no file specified, using default content",
        file_size_as_string(data_transfer.uncompressed_size),
    )

    # Encrypt data with sett.
    with prepare_files(files) as pkg_files:
        encrypt.encrypt(
            files=pkg_files,
            config=sett_config,
            recipient=data_transfer.recipients,
            sender=data_transfer.sender,
            output=data_transfer.package_file,
            dtr_id=data_transfer.route.dest.dtr_id,
            purpose=Purpose.TEST,
            passphrase=sender_pwd,
        )
    data_transfer.compressed_size = total_file_size(data_transfer.package_file)
    logger.info(
        "📦 Created package %s [%s]",
        data_transfer.package_file,
        file_size_as_string(data_transfer.compressed_size),
    )
    return data_transfer


@contextmanager
def prepare_files(files: Sequence[str]):
    if not files:
        with NamedTemporaryFile() as f:
            f.write(PKG_DEFAULT_CONTENT.encode())
            f.seek(0)
            yield [f.name]
    else:
        yield files


def send_pkg(
    data_transfer: DataTransfer,
    sett_config: SettConfig,
):
    """Send a data package to destination via the specified route.

    :param data_transfer: data to transfer.
    :param sett_config: sett configuration settings to use for the transfer.
    """

    protocol_args = asdict(data_transfer.route.via.connection)
    logger.info(
        "📨 Sending package %s %s",
        data_transfer.package_file,
        data_transfer.route_str,
    )
    transfer.transfer(
        files=[data_transfer.package_file],
        protocol=sftp(**protocol_args),
        config=sett_config,
        two_factor_callback=two_factor_cli_prompt,
        verify_dtr=False,  # already verified during packaging
    )
    logger.info("📨 transfer completed successfully")


def two_factor_cli_prompt():
    return input("Verification code: ")


def total_file_size(paths: Union[str, Sequence[str]]) -> int:
    """Computes the total size of all files/directories given in input and
    returns the size in bytes.
    """

    # If needed, convert input to a list.
    if isinstance(paths, str):
        paths = [paths]

    # Compute total size of all input files and directories.
    total_size = 0
    for path in paths:
        if os.path.isfile(path):
            total_size += os.path.getsize(path)
        else:
            for root, _, files in os.walk(path, followlinks=False):
                total_size += sum(os.path.getsize(os.path.join(root, f)) for f in files)

    return total_size


def file_size_as_string(size_in_bytes: int, precision: int = 2, sep: str = " ") -> str:
    """Converts a number of bytes into human readable units. The output units
    are selected depending on the size of the input. The function does not
    support byte values > 1000 YB.

    :param size_in_bytes: byte value to convert to human readable string.
    :param sep: separator string to use between the size value and the unit.
        e.g. if sep = " " -> "3.21 MB"
    :param precision: number of decimals to use in output.
    :return: size as human readable string.
    """

    units = ("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    log_value = int(math.log(size_in_bytes, 1024)) if size_in_bytes > 0 else 0
    return (
        str(
            round(size_in_bytes / (1024 ** log_value), precision if log_value else None)
        )
        + sep
        + units[log_value]
    )


def parse_route(destinations: Dict[str, Destination]) -> Callable[[str], Route]:
    def _parse_route(route_desc: str) -> Route:
        if "->" in route_desc:
            via_id, dest_id = route_desc.split("->")
        else:
            via_id = dest_id = route_desc
        return Route(
            dest=destinations[dest_id],
            via=destinations[via_id],
        )

    return _parse_route


def get_config():
    """Load config file from its default location"""

    conf_sub_dir_by_os: Dict[str, Tuple[str, ...]] = {
        "Linux": (".config",),
        "Darwin": (".config",),
        "Windows": ("AppData", "Roaming"),
    }

    return Config.load(
        os.path.join(
            os.path.expanduser("~"),
            *conf_sub_dir_by_os[platform.system()],
            APP_NAME,
            "config.json",
        )
    )


def read_batch_file(path: str) -> Sequence[Sequence[str]]:
    """Read a batch input file where each line corresponds to one set of data
    to encrypt and transfer, i.e. each line contains one or more files/dirs
    to packaged together.
     * On each line, the file/dir names must be comma-separated.
     * Lines starting with a # are considered comments and are ignored.
     * Comments can also be added at the end of a line.
    """

    # Load "file sets" from input file. Each "set" corresponds to a line of the
    # batch file.
    try:
        with open(path, "r", encoding="utf-8") as f:
            return [
                [expand_path(x.strip()) for x in sline.split(",")]
                for sline in (line.strip().split("#")[0].strip() for line in f)
                if sline
            ]

    except FileNotFoundError as e:
        raise ValueError(
            f"input error, unable to open --batch input file [{path}]. "
            f"Please make sure the file exists and is readable"
        ) from e


def check_files_exist(files: Sequence[str]) -> None:
    """Verify that files in the input list exist on disk"""

    missing_files = [f for f in files if not os.path.exists(f)]
    if missing_files:
        raise ValueError(
            "input error, one or more input files could not be found:"
            + "".join("\n -> " + f for f in missing_files)
        )


def expand_path(path: str) -> str:
    return os.path.realpath(os.path.expanduser(path))


def expand_paths(paths: Sequence[str]) -> List[str]:
    return [expand_path(x) for x in paths]


def cli_argument_parser(destinations: Dict[str, Destination]):
    """User input options for the CLI"""

    parser = argparse.ArgumentParser(
        description="Batch job automatization for sett", prog="BiomedIT pkg_catapult"
    )
    parser.add_argument(
        "-f",
        "--files",
        type=expand_path,
        dest="file_input",
        help="File or directory to transfer. More than one file/directory can "
        "be added by passing this option multiple times",
        action="append",
        default=None,
    )
    parser.add_argument(
        "-b",
        "--batch",
        type=read_batch_file,
        dest="batch_input",
        help="Batch mode input file. Each line in file must a comma-separated "
        "list of files/dirs to package and transfer",
        action="store",
        default=None,
    )
    parser.add_argument(
        "-s",
        "--sender",
        type=str,
        help="Email or fingerprint of data sender",
        default=None,
    )
    parser.add_argument(
        "-r",
        "--recipient",
        type=str,
        dest="recipients",
        help="Email or fingerprint of data transfer recipient",
        action="append",
    )
    parser.add_argument(
        "-k",
        "--keep-pkg",
        dest="keep_pkg",
        help="Do not delete packages after they have been transferred",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--create-only",
        dest="create_only",
        help="Do not send packages",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--tmpdir",
        type=str,
        dest="tmp_dir",
        help="Directory where to save the encrypted sett files "
        "(temporarily or permanently). "
        f"Defaults to {gettempdir()}",
        default=gettempdir(),
        action="store",
    )
    parser.add_argument(
        "--report",
        type=str,
        dest="report_file",
        help="Optional file for writing benchmark reports",
        default=None,
    )
    parser.add_argument(
        "routes",
        type=parse_route(destinations),
        nargs="*",
        help="Either a node id or two node ids separated by `->`",
    )

    return parser.parse_args()


def setup_logging():
    """Create loggers.

    A first logger is used to display info to stdout. A second logger is used
    to print summary info to a file.
    """
    logger.setLevel(logging.INFO)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(EmojiLevelFormatter("%(levelname)s  %(message)s"))
    logger.addHandler(console_handler)


if __name__ == "__main__":
    # Get user input.
    _catapult_config = get_config()
    _args = cli_argument_parser(_catapult_config.destinations)

    # Load and adapt sett configuration to use the tmp directory as the
    # location where data packages are written before they are transferred.
    _sett_cfg = _catapult_config.sett_cfg

    setup_logging()

    # Run pkg-catapult.
    if _args.batch_input is not None:
        logger.info("Running %s in batch mode", APP_NAME)
        if _args.file_input:
            logger.warning("The input passed via the -f/--files option was overriden")
        # Verify the input file contains at least 1 set of files.
        if len(_args.batch_input) == 0:
            raise ValueError("input error, no files found in the batch input file")

    main(
        routes=_args.routes,
        file_sets=_args.batch_input
        or ([_args.file_input] if _args.file_input else [[]]),
        sender=_args.sender,
        recipients=_args.recipients or [],
        keep_pkg=_args.keep_pkg,
        create_only=_args.create_only,
        sett_cfg=_sett_cfg,
        report_file=_args.report_file,
        output_dir=expand_path(_args.tmp_dir),
    )
